#include <iostream>

#include "lib/common.h"
#include "core/ClassPathDirEntry.h"
#include "core/ClassFileStream.h"
#include "core/MemorySearch.h"

int main() {
    string class_name = "com/ziya/jvm/example/HelloWorld";

    ClassPathDirEntry* e = new ClassPathDirEntry;
    ClassFileStream* stream = e->open_stream(class_name);

    INFO_PRINT("%X\n", search_u2(stream, 1));



    return 0;
}
