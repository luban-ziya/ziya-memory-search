//
// Created by ziya on 2021/9/4.
//

#include "MemorySearch.h"

void* search_u1(ClassFileStream* stream, char v) {
    char c = 0;

    while (!stream->is_end()) {
        c = stream->get_u1();

        if (c == v) {
            long p = reinterpret_cast<long>(stream->container());
            p += stream->current();
            p -= 1;

            return reinterpret_cast<void *>(p);
        }
    }

    return NULL;
}

void* search_u2(ClassFileStream* stream, short v) {
    short c = 0;

    while (!stream->is_end()) {
        c = stream->get_u2();

        if (c == v) {
            long p = reinterpret_cast<long>(stream->container());

            return (void *)(p + stream->current() - 1);
        }
    }

    return NULL;
}

void* search_u3(ClassFileStream* stream, int v) {
    return NULL;
}