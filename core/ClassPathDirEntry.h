//
// Created by ziya on 2021/5/9.
//

#ifndef ZIYA_JVM_CPP_CLASSPATHDIRENTRY_H
#define ZIYA_JVM_CPP_CLASSPATHDIRENTRY_H

#include "../lib/common.h"
#include "ClassFileStream.h"

class ClassPathDirEntry {
private:
    string _dir = "/home/ziya/IdeaProjects/ziya-jvm-teach/target/classes/";
    string _suffix = ".class";

public:
    ClassFileStream* open_stream(string class_name);

    ClassFileStream* open_file_stream(string path, string class_name);

    string& replace_all(string& str, const string& old_value, const string& new_value)
    {
        while(true)   {
            string::size_type   pos(0);
            if(   (pos=str.find(old_value))!=string::npos   ) {
                str.replace(pos, old_value.length(), new_value);
            } else {
                break;
            }
        }

        return str;
    }
};


#endif //ZIYA_JVM_CPP_CLASSPATHDIRENTRY_H
