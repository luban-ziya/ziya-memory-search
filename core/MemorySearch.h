//
// Created by ziya on 2021/9/4.
//

#ifndef ZIYA_MEMORY_SEARCH_MEMORYSEARCH_H
#define ZIYA_MEMORY_SEARCH_MEMORYSEARCH_H

#include "ClassFileStream.h"

void* search_u1(ClassFileStream* stream, char v);
void* search_u2(ClassFileStream* stream, short v);
void* search_u3(ClassFileStream* stream, int v);


#endif //ZIYA_MEMORY_SEARCH_MEMORYSEARCH_H
